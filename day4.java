package adventofcode;

import java.util.Arrays;
import java.util.Scanner;

public class day4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int answer = 0;
        while(true) {
        	
        	String s = in.nextLine();
        	
        	if(s.equals("end")) {
        		break;
        	}
        	
        	String[] words = s.split(" ");
        	boolean b =true;
        	
        	for(int i = 0 ; i < words.length ; i++) {
        		for(int j = i+1 ; j < words.length ; j++) {
        			String s1 = words[i];
        			String s2 = words[j];
        	        char[] c1 = s1.toCharArray();
        	        char[] c2 = s2.toCharArray();
        	        Arrays.sort(c1);
        	        Arrays.sort(c2);
        	        String sc1 = new String(c1);
        	        String sc2 = new String(c2);
        	        if(sc1.equals(sc2)) {
        	        	b=false;
        	        }
        		}
        	}
        	if(b) {
        		answer++;
        	}
        	
            
        }
        System.out.println(answer);
	}

}
