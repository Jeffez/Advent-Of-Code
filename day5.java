package adventofcode;

import java.util.ArrayList;
import java.util.Scanner;

public class day5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList<Integer>(); 
		
		Scanner in = new Scanner(System.in);
		int answer = 0;
		int answer2 = 0;
        while(true) {
        	
        	String s = in.nextLine();

        	if(s.equals("end")) {
        		break;
        	}
        	
        	int i = Integer.parseInt(s);
        	
        	list.add(i);
        	
        }
		
        int actual = 0;
        int count = 0;
        while(true) {
        	if(actual < 0 || actual > list.size()-1) {
        		break;
        	}
        	
        	int temp = list.get(actual);
        	// list.set(actual, temp+1);
        	list.set(actual, (temp>=3?temp-1:temp+1));
        	actual +=temp;
        	count++;
        		
        }
        System.out.println(count);
	}
	
}
