package adventofcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class day6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<int[]> states = new ArrayList<int[]>();
		
		String input = 
				"2	8	8	5	4	2	3	1	5	5	1	2	15	13	5	14";
		String[] arrayString = input.split("	");
		
		int[] array = new int[arrayString.length];
		for(int i = 0 ; i < arrayString.length ; i++) {
			array[i] = Integer.parseInt(arrayString[i]);
		}
		
		int count=0;
		int size=0;
		while(true) {

			if(contains(states, array)!=-1) {
				size=count-contains(states, array);
				break;
			}
			states.add(array.clone());
			count++;
			
			int max = 0;
			int indice = 0;
			for(int i = 0 ; i < array.length ; i++) {
				if(array[i] > max) {
					max = array[i];
					indice = i;
				}
			}
			
			array[indice] = 0;
			
			for(int i = 0 ; i < max ; i++) {
				indice++;
				if(indice > array.length-1) {
					indice=0;
				}
				array[indice]++;
			}
			
		}
		System.out.println(size);
		System.out.println(count);
	}
	
	
	public static int contains(ArrayList<int[]> states, int[] array) {
		
		for(int[] array2 : states) {
			boolean b = true;
			for(int i = 0; i < array.length ; i++) {
				if(array[i] != array2[i]) {
					b=false;
					break;
				}
			}
			if(b) {
				return states.indexOf(array2);
			}
		}
		return -1;
	}
}
